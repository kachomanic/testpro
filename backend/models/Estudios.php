<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "estudios".
 *
 * @property integer $codEstudio
 * @property string $lugar
 * @property integer $codTipoEstudio
 *
 * @property TipoEstudios $codTipoEstudio0
 * @property EstudiosEstudiantes[] $estudiosEstudiantes
 */
class Estudios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estudios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lugar', 'codTipoEstudio'], 'required'],
            [['codTipoEstudio'], 'integer'],
            [['lugar'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codEstudio' => 'Cod Estudio',
            'lugar' => 'Lugar',
            'codTipoEstudio' => 'Cod Tipo Estudio',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodTipoEstudio0()
    {
        return $this->hasOne(TipoEstudios::className(), ['codTipoEstudio' => 'codTipoEstudio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstudiosEstudiantes()
    {
        return $this->hasMany(EstudiosEstudiantes::className(), ['codEstudio' => 'codEstudio']);
    }
}
