<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "familias".
 *
 * @property integer $codFamilia
 * @property integer $codProfesion
 * @property string $nombre
 * @property boolean $sexo
 *
 * @property Profesiones $codProfesion0
 * @property FamiliasEstudiantes[] $familiasEstudiantes
 */
class Familias extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'familias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codProfesion', 'nombre'], 'required'],
            [['codProfesion'], 'integer'],
            [['sexo'], 'boolean'],
            [['nombre'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codFamilia' => 'Cod Familia',
            'codProfesion' => 'Cod Profesion',
            'nombre' => 'Nombre',
            'sexo' => 'Sexo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodProfesion0()
    {
        return $this->hasOne(Profesiones::className(), ['codProfesion' => 'codProfesion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamiliasEstudiantes()
    {
        return $this->hasMany(FamiliasEstudiantes::className(), ['codFamilia' => 'codFamilia']);
    }
}
