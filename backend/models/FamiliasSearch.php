<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Familias;

/**
 * FamiliasSearch represents the model behind the search form about `backend\models\Familias`.
 */
class FamiliasSearch extends Familias
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codFamilia', 'codProfesion'], 'integer'],
            [['nombre'], 'safe'],
            [['sexo'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Familias::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'codFamilia' => $this->codFamilia,
            'codProfesion' => $this->codProfesion,
            'sexo' => $this->sexo,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }
}
