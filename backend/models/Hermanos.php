<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "hermanos".
 *
 * @property integer $codHermano
 * @property integer $codEstudiante
 * @property string $cantHermanos
 * @property string $cantHermanas
 *
 * @property Estudiantes $codEstudiante0
 */
class Hermanos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hermanos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codEstudiante', 'cantHermanos', 'cantHermanas'], 'required'],
            [['codEstudiante'], 'integer'],
            [['cantHermanos', 'cantHermanas'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codHermano' => 'Cod Hermano',
            'codEstudiante' => 'Cod Estudiante',
            'cantHermanos' => 'Cant Hermanos',
            'cantHermanas' => 'Cant Hermanas',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodEstudiante0()
    {
        return $this->hasOne(Estudiantes::className(), ['codEstudiante' => 'codEstudiante']);
    }
}
