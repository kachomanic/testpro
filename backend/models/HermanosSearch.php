<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Hermanos;

/**
 * HermanosSearch represents the model behind the search form about `backend\models\Hermanos`.
 */
class HermanosSearch extends Hermanos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codHermano', 'codEstudiante'], 'integer'],
            [['cantHermanos', 'cantHermanas'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Hermanos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'codHermano' => $this->codHermano,
            'codEstudiante' => $this->codEstudiante,
            'cantHermanos' => $this->cantHermanos,
            'cantHermanas' => $this->cantHermanas,
        ]);

        return $dataProvider;
    }
}
