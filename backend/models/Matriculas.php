<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "matriculas".
 *
 * @property integer $codMatricula
 * @property string $semestre
 * @property string $añoAcademico
 * @property string $fecha
 * @property integer $codFacultad
 *
 * @property Facultades $codFacultad0
 * @property Notas[] $notas
 */
class Matriculas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'matriculas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['semestre', 'añoAcademico', 'fecha', 'codFacultad'], 'required'],
            [['semestre'], 'number'],
            [['fecha'], 'safe'],
            [['codFacultad'], 'integer'],
            [['añoAcademico'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codMatricula' => 'Cod Matricula',
            'semestre' => 'Semestre',
            'añoAcademico' => 'Año Academico',
            'fecha' => 'Fecha',
            'codFacultad' => 'Cod Facultad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodFacultad0()
    {
        return $this->hasOne(Facultades::className(), ['codFacultad' => 'codFacultad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotas()
    {
        return $this->hasMany(Notas::className(), ['codMatricula' => 'codMatricula']);
    }
}
