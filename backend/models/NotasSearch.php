<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Notas;

/**
 * NotasSearch represents the model behind the search form about `backend\models\Notas`.
 */
class NotasSearch extends Notas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codGrupo', 'codMatricula', 'codEstudiante'], 'integer'],
            [['presencial_especial', 'tutoria'], 'boolean'],
            [['nota'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Notas::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'codGrupo' => $this->codGrupo,
            'codMatricula' => $this->codMatricula,
            'codEstudiante' => $this->codEstudiante,
            'presencial_especial' => $this->presencial_especial,
            'tutoria' => $this->tutoria,
            'nota' => $this->nota,
        ]);

        return $dataProvider;
    }
}
