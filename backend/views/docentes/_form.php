<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Docentes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="docentes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'carnetDocente')->textInput(['maxlength' => 15]) ?>

    <?= $form->field($model, 'nombres')->textInput(['maxlength' => 70]) ?>

    <?= $form->field($model, 'apellidos')->textInput(['maxlength' => 70]) ?>

    <?= $form->field($model, 'cedula')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => 40]) ?>

    <?= $form->field($model, 'correo')->textInput(['maxlength' => 40]) ?>

    <?= $form->field($model, 'especialidad')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
