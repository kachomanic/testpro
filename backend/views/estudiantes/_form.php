<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Estudiantes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estudiantes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'carnetEst')->textInput(['maxlength' => 15]) ?>

    <?= $form->field($model, 'fechaIngreso')->textInput() ?>

    <?= $form->field($model, 'codParroquia')->textInput() ?>

    <?= $form->field($model, 'teleDomicilio')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'direccionDomicilio')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'cedula')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'fechaNac')->textInput() ?>

    <?= $form->field($model, 'lugarNac')->textInput(['maxlength' => 70]) ?>

    <?= $form->field($model, 'sApellido')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'pApellido')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'sNombre')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'pNombre')->textInput(['maxlength' => 50]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
