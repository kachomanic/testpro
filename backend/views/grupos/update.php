<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Grupos */

$this->title = 'Update Grupos: ' . ' ' . $model->codGrupo;
$this->params['breadcrumbs'][] = ['label' => 'Grupos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codGrupo, 'url' => ['view', 'id' => $model->codGrupo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="grupos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
