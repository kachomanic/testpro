<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Matriculas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="matriculas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'semestre')->textInput(['maxlength' => 1]) ?>

    <?= $form->field($model, 'añoAcademico')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'codFacultad')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
