<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Matriculas */

$this->title = 'Update Matriculas: ' . ' ' . $model->codMatricula;
$this->params['breadcrumbs'][] = ['label' => 'Matriculas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codMatricula, 'url' => ['view', 'id' => $model->codMatricula]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="matriculas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
