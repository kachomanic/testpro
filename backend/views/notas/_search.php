<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\NotasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codGrupo') ?>

    <?= $form->field($model, 'codMatricula') ?>

    <?= $form->field($model, 'codEstudiante') ?>

    <?= $form->field($model, 'presencial_especial')->checkbox() ?>

    <?= $form->field($model, 'tutoria')->checkbox() ?>

    <?php // echo $form->field($model, 'nota') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
