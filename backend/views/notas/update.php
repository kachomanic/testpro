<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Notas */

$this->title = 'Update Notas: ' . ' ' . $model->codGrupo;
$this->params['breadcrumbs'][] = ['label' => 'Notas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codGrupo, 'url' => ['view', 'codGrupo' => $model->codGrupo, 'codMatricula' => $model->codMatricula, 'codEstudiante' => $model->codEstudiante]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="notas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
