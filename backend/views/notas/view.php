<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Notas */

$this->title = $model->codGrupo;
$this->params['breadcrumbs'][] = ['label' => 'Notas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codGrupo' => $model->codGrupo, 'codMatricula' => $model->codMatricula, 'codEstudiante' => $model->codEstudiante], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codGrupo' => $model->codGrupo, 'codMatricula' => $model->codMatricula, 'codEstudiante' => $model->codEstudiante], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codGrupo',
            'codMatricula',
            'codEstudiante',
            'presencial_especial:boolean',
            'tutoria:boolean',
            'nota',
        ],
    ]) ?>

</div>
