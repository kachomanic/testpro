<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Parroquias */

$this->title = 'Update Parroquias: ' . ' ' . $model->codParroquia;
$this->params['breadcrumbs'][] = ['label' => 'Parroquias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codParroquia, 'url' => ['view', 'id' => $model->codParroquia]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="parroquias-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
